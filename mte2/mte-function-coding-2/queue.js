let collection = [];

// Write the queue functions below.

function print() {
    return collection;
}

function enqueue(element) {
    collection[collection.length] = element;
    return print();
}

function dequeue() {
    if (isEmpty()) {
        return undefined;
    }
    let frontElement = collection[0];
    for (let i = 0; i < collection.length - 1; i++) {
        collection[i] = collection[i + 1];
    }
    collection.length--;
    return print();
}

function front() {
    if (isEmpty()) {
        return undefined;
    }
    return collection[0];
}

function size() {
    return collection.length;
}


function isEmpty() {
    return collection.length === 0;
}

module.exports = {
    collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};