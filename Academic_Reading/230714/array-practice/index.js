//activity #1
let color = ["Blue","Red","Yellow","Orange","Green","Purple"];

console.log("--Write a simple JS function to join all elements of the following array into a string--")
console.log("Original Array: ")
console.log(color)

let colors = color.join();

console.log("Result:")
console.log(colors);
console.log(); //just a space

//activity #2
let planet = ["Venus","Mars","Earth"];

console.log("--Create a function that will add a planet at the beginning of the array--");
console.log("Original Array: ")
console.log(planet)

function addElement(element) {
    return planet.unshift(element)
}

addElement("Saturn");
console.log("Result:");
console.log(planet);
console.log(); //just a space

//activity #3
let singers = ["Moira", "Taylor", "Jed", "Regine"];

console.log("--Create an array of your favorite singers' names. Create a function that will remove the last name in the array. Then, replace the deleted name with other two names.--");
console.log("Original Array: ")
console.log(singers)

function deleteAdd(singer1, singer2) {
    singers.length--;
    singers[singers.length] = singer1;
    singers[singers.length] = singer2;
}

deleteAdd("JB", "Freia")
console.log("Result:")
console.log(singers);
