let getGrade = () => prompt("Enter a grade: ");

let grade = parseInt(getGrade());


if (grade < 80) {
	console.log("Your grade is " + grade + ". You are Novice.");
} else if (grade >= 81 && grade <= 90) {
	console.log("Your grade is " + grade + ". You are Intermediate.");
} else if (grade >= 91 && grade <= 100) {
	console.log("Your grade is " + grade + ". You are Advance.");
} else {
	console.log("Invalid grade!");
}

