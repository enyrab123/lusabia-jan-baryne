function oddEvenChecker() {
	for (let x = 1; x <= 50; x++) {
		if (x % 2 === 0) {
			console.log("The number " + x + " is even.")
		} else {
			console.log("The number " + x + " is odd.")
		}
	}
}//end of oddEvenChecker

oddEvenChecker();