const word = document.getElementById("text").innerHTML;

console.log("The orignal word:");
console.log(word);

function wordCount(word) {
	// Remove leading and trailing whitespaces
	const trimmedWord = word.trim();

  	// Split the trimmedWord into an array of words using whitespace as the delimiter
	const wordsArray = trimmedWord.split(/\s+/);

  	// Return the length of the wordsArray, which represents the number of words
	return wordsArray.length;
}

const count = wordCount(word);
console.log("Word count:", count);