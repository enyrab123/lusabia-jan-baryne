
	let fruits = ["Apple", "Orange", "Kiwi", "Dragon Fruit"];

	console.log("Current array:");
	console.log(fruits);

	let fruitsLength = fruits.push("Mango");

	console.log(fruitsLength);
	console.log("Mutated array from push method:");
	console.log(fruits);

	fruits.push("sadas", "dasda");
	console.log(fruits);

	let removedFruit = fruits.pop();
	console.log(removedFruit);
	console.log(fruits);

	function removeFruit() {
		fruits.pop();
	}

	removeFruit();
	console.log(fruits)

	fruits.unshift("Grapes");
	console.log(fruits)
	fruits.shift("Grapes");
	console.log(fruits)

	fruits.splice(1, 2, "Santol", "Cherry");
	console.log(fruits)

	fruits.sort();
	fruits.reverse();
	console.log(fruits)