/*
	Servers can actually respond differently with different request

	We start our request with our URL
	A client can start a different request with a different URL

	http://localhost:4000/
	is not the same
	http://localhost:4000/profile

	/ = url endpoint (default)
	/profile = url endpoint
*/

const http = require("http"); 
const port = 4000; //creates a variable to store the port number

const app = http.createServer((request, response) => {
	/*
		Information about the URL endpoint of the req is in the request object
		Different request requires different response
		The process or way to respond differently to a request is called a ROUTE
	*/

	if (request.url == '/greeting') {
		response.writeHead(200, {'Content-type': 'text/plain'})
		response.end('Hello, B297')
	} else if (request.url == '/homepage') {
		response.writeHead(200, {'Content-type': 'text/plain'})
		response.end('This is the homepage')
	} else {
		response.writeHead(404, {'Content-type': 'text/plain'})
		response.end('Page not available ,try other routes')
	}
})

app.listen(port)

console.log(`Server is now accessible at localhost: ${port}`)