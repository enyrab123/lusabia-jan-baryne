
	/*function printName() {
		let nickname = prompt("Enter your nickname: ");
		console.log("Hi, " +  nickname);
	}

	printName();*/

	function printName(name) {
		console.log("Hi, " +  name);
	}

	printName("Cee");

	function checkDivisibilityBy4(num) {
		let remainder = num % 4;
		let isDivisibleBy4 = remainder === 0;
		console.log("Is " + num + " divisible by 4?")
		console.log(isDivisibleBy4);
	}

	checkDivisibilityBy4(56);
	checkDivisibilityBy4(95);
	checkDivisibilityBy4(444);


	function printFriends(friend1, friend2, friend3) {
		console.log("My three friends are: " + friend1 + "," + friend2 + "," + friend3);
	}


	printFriends("Amy", "Lulu", "Morgan");
	
	//calculate square
	function calculateSquareArea(sideLength) {
	  let area = sideLength * sideLength;

	  return area;
	}

	let sideLength = 4;
	let area = calculateSquareArea(sideLength);
	console.log("The area of the square with a side length of 4 is: " + area);

	//add 3 numbers
	function add3Numbers(x, y, z) {
		return x + y + z;
	} 

	let x = 1;
	let y = 2;
	let z = 3;

	console.log("The sum of " + x + ", " + y + ", " + z + " is " + add3Numbers(x, y, z));

	//is equal to 100
	function isEqualTo100(num) {
		return num === 100
	}

	let num = 99;

	console.log(num + " is equal to 100: " + isEqualTo100(num));