/*
	> npm init - used to initialize a new package.json file
	> package.json - contains info about the project, dependencies, and other settings
	> package-lock.json - locks the versions of all installed dependencies to its specific version
*/

const express = require("express")
const app = express();
const port = 4000;

//Middleware - a software that provides common services and capabilities to application outside of what's offered by operatiing system

//app.use() is a method used to run another function or method for our expressjs api
//it is used to run middlewares

//allows our app to read json data
app.use(express.json())
//allows our app to read data from forms
//this is also allows us to recieve information in other data types such as an object which we will use throughout our application 
app.use(express.urlencoded({extended:true}))

app.get("/", (req, res) => {
	res.send("Hello World")
})

app.get("/hello", (req, res) => {
	res.send("Hello from /hello endpoint")
})

//Refactor the post route to recieve data from the req.body
//Send a message that has the data recieved
	//`Hello firstName lastName`
app.post("/hello", (req, res) => {
	//res.send("Hello from the post route")

	res.send(`Hello there ${req.body.firstName} ${req.body.lastName}`)
})

app.put("/", (req, res) => {
	res.send("hello from the ExpressJS PUT method route!")
})

app.delete("/", (req, res) => {
	res.send("hello from the ExpressJS DELETE method route!")
})

//array will store objects when the '/signup' route is accessed
let users = []

//MA3
	//refactor the signup route
	//if content of the req.body with the property of username aand password is not an empty string, this will store the user object via Postman to the users array
		//send a response back to the client/Postman after the request has been proccessed
	//else, id the username and password are not complete, an error message will be sent back to the client/Postman
		//please input both username and password
app.post("/signup", (req, res) => {
	console.log(req.body)

	if (req.body.username != '' && req.body.password != '') {
		users.push(req.body)
		res.send("User successfully signed up!")

	} else {
		res.send("Please input both username and password")
	}
})//end of signup

app.put("/change-password", (req, res) => {
	let message;

	for (let i = 0; i < users.length; i++) {
		if (req.body.username == users[i].username) {
			users[i].password = req.body.password;

			message = `User ${req.body.username}'s password has been updated!`
			console.log(users);
			break;
		} else {
			message = "User does not exist"
		}
	} 
	res.send(message);
})//end of change-password

//start of activity

	//code here


//end of activity

if (require.main === module) {
	app.listen(port, () => console.log(`Server running at port ${port}`))
}
module.exports = app;




