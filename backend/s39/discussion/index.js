/*
	JavaScript Synchronous vs Asynchronous

	JS by default is synchronous meaning that only one statement is executed at a time

	Asynchronous means that we can proceed to execute other statements, while time consuming code is running in the background
*/

/*
	[Getting All Posts]
	The Fetch API allows us to asynchronously request for a resource (data)
	A "promise" is an OBJECT that represents the eventual completion (or failure) of a async function and its resulting value

	Promises is like a special container for a future value that not be available immediately. The promise represents the outcome fo an async operation that may take some tiime to complete, such as fetching data from a server, reading a file, getting data from the database and any other tasks that doesnt happen instantly.

	Syntax
	fetch('URL')
*/
console.log(fetch('https://jsonplaceholder.typicode.com/posts'))

/*
	the fetch)_ function returns a Promise which cab the be chained usingh the .then() function. The then() function waits for the promise to be resolved before executing the code

	Syntax
	fetch('URL')
	.then((response) => {})
*/

fetch('https://jsonplaceholder.typicode.com/posts')
.then(response => console.log(response.status))

fetch('https://jsonplaceholder.typicode.com/posts')
//Use the 'json' method from the "Response" object to CONVERT the data retrieved into JSON format to be used in our application
.then((response) => response.json())
//log the converted JSON value from the "fetch" request
//When we have multiple .then methods, it creates a promise chain
.then((posts) => console.log(posts))        

async function fetchData() {
	//waits for the "fetch" method to complete then stores the value in the "result" variable
	let result = await fetch('https://jsonplaceholder.typicode.com/posts')
	console.log(result);
	console.log(typeof result);
	console.log(result.body);

	//convert the data from the response object as JSON
	let json = await result.json()
	console.log(json);
}

fetchData()

//[Retrieves a specific post]
fetch('https://jsonplaceholder.typicode.com/posts/18')
.then((response) => response.json())
.then((json) => console.log(json))   

//[Creating a post]
fetch('https://jsonplaceholder.typicode.com/posts', {

	method: 'POST',
	headers: {
		'Content-type': 'application/json'
	},
	body: JSON.stringify({
		title: 'New post',
		body: 'Hello World!',
		userId: 1
	})
})//end of fetch
.then((response) => response.json())
.then((json) => console.log(json));

//[Updating a post]
fetch('https://jsonplaceholder.typicode.com/posts/1', {

	method: 'PUT',
	headers: {
		'Content-type': 'application/json'
	},
	body: JSON.stringify({
		title: 'Corrected post'
	})
})//end of fetch
.then((response) => response.json())
.then((json) => console.log(json));

//[Deleting a post]
fetch('https://jsonplaceholder.typicode.com/posts/1', {
	method: 'DELETE'
})

//[Filtering post]
fetch('https://jsonplaceholder.typicode.com/posts?userId=5&userId=2')
.then((response) => response.json())
.then((json) => console.log(json));

//[Retrieve nested/related comments to posts]
fetch('https://jsonplaceholder.typicode.com/posts/1/comments')
.then((response) => response.json())
.then((json) => console.log(json));