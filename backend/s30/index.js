console.log("ES6 UPDATES!");

//ES6 Updates
	//ES6 is one of the latest versions of writing JS and in fact ONE of the MAJOR updates

	//let and const
		//are ES6 updates, these are the new standards of creating variables

	//In JS, hoisting allows us to use functions and variables before they are declared
	//BUT this might cause confusion, because of the confusion that var hoisting can create, it is BEST to AVOID using Variables before they are declared

	console.log(varSample);//undefined
	var varSample = "Hoist me up!!";

	//if you have used nameVar in other parts of the code, you might be surprised at the output we might get

	var nameVar = "Camille";

	if(true){
		var nameVar = "Hi!"
	}
	var nameVar = "C";

	console.log(nameVar);

	let name1 = "Cee";

	if(true){
		let name1 = "Hello";
	}

	//let name1 = "Hello World";

	console.log(name1);

//[** Exponent Operator]
	
	//Update
	const firstNum = 8**2;
	console.log(firstNum);//64

	const secondNum = Math.pow(8,2);
	console.log(secondNum);//64

	let string1 = 'fun';
	let string2 = 'Bootcamp';
	let string3 = 'Coding';
	let string4 = 'JavaScript';
	let string5 = 'Zuitt';
	let string6 = 'love';
	let string7 = 'Learning';
	let string8 = 'I';
	let string9 = 'is';
	let string10 = 'in';

/*
	MA1 (3min)
	1. Create a new variable called concatSentence1
	2. Concatentate and save a resulting string into concatSentence1
	3. Log the concatSentence1 in your console and take a screenshot
	**the sentences MUST have spaces and punctuation
*/
//[Template Literals`${}`]
/*
	Allows us to write strings without using the concatentation operator (+)
	Greatly helps with code readability
*/
	let concatSentence = string8 + " " + string6 + " " + string5;
	console.log(concatSentence);

	let concatSentence2 = `${string8} ${string6} ${string5} ${string3} ${string2}!`
	console.log(concatSentence2);

/*
	concat method for strings
	let sentence = concatSentence.concat(". ",concatSentence2);
	console.log(sentence);
*/
//${} is a placeholder that is used to embed JS expressions when creating strings using template literals
	let name = "Carding";

	let message = `Hello ${name}! Welcome to programming!`;
	console.log(`Message with template literals: ${message}`);

//multi-line using template literals
	const anotherMessage = `
	
	${name} attended a math competition.

	He won it by solving the problem 8**2 with the solution of ${firstNum}!
	`
	console.log(anotherMessage);

	const interestRate = .1;
	const principal = 1000;
	console.log(`The interest on your savings account is: ${principal * interestRate}`)

	let dev = {
		name: "Peter",
		lastName: "Parker",
		occupation: "Web developer",
		income: 50000,
		expenses: 60000
	};

	console.log(`${dev.name} is a ${dev.occupation}.`);
	console.log(`His income is ${dev.income} and expenses at ${dev.expenses}. His current balance is ${dev.income - dev.expenses}`);

// [Array Destructuring]
/*
	Allow us to unpack elements in arrays into distinct variables
	Allows us to name array elements with variables instead of using index numbers
	Helps with code readability
	
	Syntax
		let/const [variableName, variableName, variableName] = array;

*/

	let fullName = ["Juan","Dela","Cruz"];
//Pre-Array Destructuring
	console.log(fullName[0]);
	console.log(fullName[1]);
	console.log(fullName[2]);
	console.log(`Hello, ${fullName[0]} ${fullName[1]} ${fullName[2]}! It's nice to meet you!`);

//Array Destructing

	let [firstName, middleName, lastName] = fullName;
	console.log(firstName);
	console.log(middleName);
	console.log(lastName);
	console.log(`Hello, ${firstName} ${middleName} ${lastName}! I was enchanted to meet you!`);

	console.log(fullName);

	let fruits = ["Mango","Grapes","Guava","Apple"];
	let [fruit1, , fruit3, ] = fruits;
	console.log(fruit1);
	console.log(fruit3);
// MA2
	//destructure this array and log the individual variables
	//stretch goal, do not assign a variable name for Taguro

	let kupunanNiEugene = ["Eugene","Alfred","Vincent","Dennis","Taguro","Master Jeremiah"];
	let [fighter1, fighter2, fighter3, fighter4,, fighter5] = kupunanNiEugene;
	console.log(` ${fighter1} ${fighter2} ${fighter3} ${fighter4} ${fighter5}`)

//[Object Destructuring]
/*
	Allows to unpack properties of objects into distinct variables
	Shortens the syntax for accessing properties from objects
	Syntax:
		let/const {propertyName, propertyName, propertyName} = object;

*/

	const person = {
		givenName: "Jane",
		maidenName: "Dela",
		familyName: "Cruz"
	}

//pre-object destructuring
	console.log(person.givenName);
	console.log(person.maidenName);
	console.log(person.familyName);
	console.log(`Hello ${person.givenName} ${person.maidenName} ${person.familyName}! It's good to see you again!`)

//Object Destructuring
	const { givenName, maidenName, familyName } = person;

	console.log(givenName);
	console.log(maidenName);
	console.log(familyName);

	console.log(`Hello ${givenName} ${maidenName} ${familyName}! It's good to see you again!`)

	function getFullName ({givenName, maidenName, familyName}){
		console.log(`${givenName} ${maidenName} ${familyName}`)
	}

	getFullName(person);

//[Arrow functions]

/*
	Compact alternative syntax to traditional functions
	Useful for code snippets where creating functions will not be reused in any other portion of the code
	Adhere to "DRY" (Don't Repeat Yourself) principle where there's no longer need to create a function and think of a name for functions that will only be used in certain code snippets
*/

	function displayMsg(){
		console.log("Hi");
	}

	displayMsg();

	let displayHello = () => {
		console.log("Hello World!")
	}

	displayHello();

	/*function printFullName (firstName,middleInitial,lastName){
		console.log(`${firstName} ${middleInitial} ${lastName}`);
	}

	printFullName("John","D","Smith");*/

	let printFullName = (firstName,middleInitial,lastName) => {
		console.log(`${firstName} ${middleInitial} ${lastName}`)
	}

	printFullName('John',"D",'Smith');

	const students = ["John","Jane","Natalia","Jobert","Joe"];

	//arrow functions with loops

	//pre-arrow
	students.forEach(function(student){
		console.log(`${student} is a student!`);
	})

	//arrow
	students.forEach((student)=>{
		console.log(`${student} is a student. (from arrow)`)
	})

//[Implicit Return Statement]
/*
	There are instances when you can omit the "return" statement
	This works because even without the "return" statement JS implicitly adds it for the result of the function
*/

	/*function add (x,y){
		return x + y;
	}

	let total = add(1,2);
	console.log(total);//3*/

	/*const add = (x,y) =>{
		return x + y;
	}

	let total = add(1,2);
	console.log(total);*/

	const add = (x,y) => x + y;

	let total = add(1,2);
	console.log(total);

//[Default Function Argument Value]

	const greet = (name = 'User') =>{
		return `Good morning, ${name}!`
	}

	console.log(greet());
	console.log(greet("John"));

//[Class-Based Object Blueprints]
/*
	Allows creation/instantiation of objects using classes as blueprints
*/

//Creating a class
/*
	- The constructor is a special method of a class for creating/initializing an object for that class
	- "this" keyword refers to the properties of an object created/initialized from the class

*/
	class Car {
		constructor(brand,name,year){
			this.brand = brand;
			this.name = name;
			this.year = year;
		}
	}

	let myCar = new Car();

	console.log(myCar);

	myCar.brand = "Ford";
	myCar.name = "Ranger Raptor";
	myCar.year = 2021

	console.log(myCar);

	const myNewCar = new Car("Toyota", "Vios", 2021);

	console.log(myNewCar);

	let character1 = {
		name: "Cloud Strife",
		occupation: "Soldier",
		greet: function() {
			console.log(`Hi! I'm ${this.name}`);
		}
	}//end of character1

	character1.greet();


	class Character {
		constructor(name, role, strength, weakness) {
			this.name = name;
			this.role = role;
			this.strength = strength;
			this.weakness = weakness;
		}
	}


	