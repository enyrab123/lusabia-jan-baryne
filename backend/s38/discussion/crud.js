let http = require("http");
let port = 4000;

let directory = [
{
	"name": "Zara Evergreen",
	"email": "zara/evergreen@gmail.com"
}, 
{
	"name": "Joy Boy",
	"email": "joyboy@gear5.com"
}
]

let app = http.createServer((request, response) => {
	if (request.url == "/users" && request.method == "GET") {
		response.writeHead(200, {'Content-type': 'application/json'})
		response.write(JSON.stringify(directory))
		//send data to client
		response.end();
		//response.end finalizes the response
	} else if (request.url == "/users" && request.method == "POST") {
		//route to add a new user, we have to recieve an input from the client
		//to be able to recieve he request body or the input from the request/client, we have to add a  way to recieve that input
		//In NodeJS, this is done in two steps


		//requestBody will be a placeholder to contain the data (request body) passed from the client
		let requestBody = '';

		/*
			1st step in recieving data from the request in NodeJs is called the data step

			data step - will read the incoming stream of data from the client and process it so we can save it in the requestBody variable\
		*/

		request.on('data', (data) => {
			console.log(`This is the data recived from the client ${data}`)
			requestBody += data; 
		})

		/*
			end step = this will run once or after the request data has been completely sent from the client
		*/

		//initially, requestBody is in JSON format. We cannot add this to our directory array because it is a string... So we have to update requestBody variable with a parsed version of the recieved JSON format data
		request.on('end', () => {
			console.log(`This is the requestBody ${requestBody}`)
			console.log(typeof requestBody)

			requestBody = JSON.parse(requestBody)

			let newUser = {
				"name": requestBody.name,
				"email": requestBody.email
			}
			console.log(requestBody);
			console.log(typeof requestBody);
			console.log(`This is the email: ${requestBody.email}`)

			directory.push(newUser)
			console.log(`This is the email: ${requestBody.email}`)

			response.writeHead(200, {'Content-type': 'application/json'})
			response.write(JSON.stringify(directory))
			//response.write(JSON.stringify(newUser.name))
			response.end()
		})
	}//end of else
})//end of createServer

app.listen(port, () => console.log(`Server running at localhost:${port}`))