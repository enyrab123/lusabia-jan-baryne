const express = require("express");
const courseController = require("../controllers/course");
const auth = require("../auth");
const {verify, verifyAdmin} = auth;

//Routing component
const router = express.Router();

//create a course
router.post("/", verify, verifyAdmin, courseController.addCourse);

//route for retrieving all course
router.get("/all", courseController.getAllCourse);

//create a for getting all active courses
//use default endpoint
//getAllActiveCourses
router.get("/", courseController.getAllActiveCourses);

//get a specific course
router.get("/:courseId", courseController.getCourses);

//edit a specific course
router.put("/:courseId", verify, verifyAdmin, courseController.updateCourse);

//archive a course
router.put("/:courseId/archive", verify, verifyAdmin, courseController.archiveCourse);

//activate a course
router.put("/:courseId/activate", verify, verifyAdmin, courseController.activateCourse);

// Route to search for courses by course name
router.post('/search', courseController.searchCoursesByName);

// route to get all enrolled user to a certain course
router.get('/:courseId/enrolled-users', courseController.getEmailsOfEnrolledUsers)

//search course by price range
router.post('/searchByPrice', courseController.searchCoursesByPriceRange);



//Export route system
module.exports = router;