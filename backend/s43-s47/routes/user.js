//Route

//Dependensices and modules

const express = require("express");
const userController = require("../controllers/user");
const auth = require("../auth");
const {verify, verifyAdmin} = auth;



//Routing component
const router = express.Router();

//Routes - POST

//to check email
router.post("/checkEmail", (req, res) => {
	userController.checkEmailExist(req.body).then(resultFromController => res.send(resultFromController))
})

//register a user
/*router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
})*/
router.post("/register", userController.registerUser);

//User authentication
router.post("/login", userController.loginUser);

//retrieve user details
router.post("/details", verify, userController.getProfile);

//route to enroll user to a course
router.post("/enroll", verify, userController.enroll);

//getEnrollments of user
router.get("/getEnrollments", verify, userController.getEnrollments);

//post router for resetting the password
router.post('/reset-password', verify, userController.resetPassword);

// Update user profile route
router.put('/profile', verify, userController.updateProfile);

//update user as admin
router.put('/updateAdmin', verify, verifyAdmin, userController.updateUserAsAdmin);

//update user as admin
router.put('/enrollmentStatusUpdate', verify, verifyAdmin, userController.updateEnrollmentStatus);

module.exports = router;