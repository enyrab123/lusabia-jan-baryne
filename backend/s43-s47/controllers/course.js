const Course = require("../models/Course");
const User = require("../models/User");

module.exports.addCourse = (req, res) => {
	let newCourse = new Course({
		name : req.body.name,
		description : req.body.description,
		price : req.body.price
	})
	//saves the created object to our database
	return newCourse.save().then((course, error) => {
		if (error) {
			return res.send(false);
		} else {
			return res.send(true);
		}
	})

	.catch(err => res.send(err))
}//end of addCourse

//retrieve all courses
/*
	1. Retrieve all the courses from the database
*/

//we will use the find() method for our course model

module.exports.getAllCourse = (req, res) => {
	return Course.find({}).then(result => {
		return res.send(result);
	})
}//end of getAllCourse

//getAllactiveCourses
//create a function that will handle req, res that will all active courses in the db
module.exports.getAllActiveCourses = (req, res) => {
	return Course.find({isActive: true}).then(result => {
		return res.send(result);
	})
}//end of getAllActiveCourses

//get specific course
module.exports.getCourses = (req, res) => {
	return Course.findById(req.params.courseId).then(result => {
		return res.send(result);
	})
}//end of getCourses

//update specific course
module.exports.updateCourse = (req, res) => {
	let updatedCourse = {
		name: req.body.name,
		description: req.body.description,
		price: req.body.price
	}

	return Course.findByIdAndUpdate(req.params.courseId, updatedCourse).then((course, error) => {
		if (error) {
			return res.send(false);
		} else {
			return res.send(true);
		}
	})
}//end of updateCourse

//archive a course
module.exports.archiveCourse = (req, res) => {
	let updateActiveField = {
        isActive: false
    }

	return Course.findByIdAndUpdate(req.params.courseId, updateActiveField).then((course, error) => {
		if (error) {
			return res.send(false);
		} else {
			return res.send(true);
		}
	})
}//end of archiveCourse

//archive a course
module.exports.activateCourse = (req, res) => {
	let updateActiveField = {
        isActive: true
    }

	return Course.findByIdAndUpdate(req.params.courseId, updateActiveField).then((course, error) => {
		if (error) {
			return res.send(false);
		} else {
			return res.send(true);
		}
	})
}//end of activateCourse

// Controller action to search for courses by course name
module.exports.searchCoursesByName = async (req, res) => {
  try {
    const { courseName } = req.body;

    // Use a regular expression to perform a case-insensitive search
    const courses = await Course.find({
      name: { $regex: courseName, $options: 'i' }
    });

    res.json(courses);
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Internal Server Error' });
  }
};

// Controller action to get emails of  enrolled users
module.exports.getEmailsOfEnrolledUsers = async (req, res) => {
  const courseId = req.params.courseId; // Use req.params to get the courseId from the URL

  try {
    // Find the course by courseId
    const course = await Course.findById(courseId);

    if (!course) {
      return res.status(404).json({ message: 'Course not found' });
    }

    // Get the userIds of enrolled users from the course
    const userIds = course.enrollees.map(enrollee => enrollee.userId);

    // Find the users with matching userIds
    const enrolledUsers = await User.find({ _id: { $in: userIds } }); // Use userIds instead of users

    // Extract the emails from the enrolled users
    const emails = enrolledUsers.map(user => user.email); // Use map() instead of forEach()

    res.status(200).json({ userEmails: emails }); // Use userEmails instead of emails
  } catch (error) {
    res.status(500).json({ message: 'An error occurred while retrieving enrolled users' });
  }
};

// Controller to search courses based on price range
module.exports.searchCoursesByPriceRange = async (req, res) => {
    try {
      const { minPrice, maxPrice } = req.body;

      if (!minPrice || !maxPrice) {
        return res.status(400).json({ message: 'Both minPrice and maxPrice are required in the request body.' });
      }

      const courses = await Course.find({
        price: { $gte: minPrice, $lte: maxPrice },
      });

      return res.json({ courses: courses });
    } catch (error) {
      console.error(error);
      return res.status(500).json({ message: 'An error occurred while searching for courses.' });
    }
  };