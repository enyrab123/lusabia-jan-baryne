const User = require("../models/User");
const Course = require("../models/Course");
const bcrypt = require("bcrypt");
const auth = require("../auth")

//Check if the email exist already
/*
	Steps:
	1. "find" mongoose method to find duplicate items
	2. "then method" to send a response back to the FE application based on the result of the "find" method
*/

module.exports.checkEmailExist = (reqbody) => {
	return User.find({email: reqbody.email}).then(result => {
		//find method returns a record if a match is found
		if (result.length > 0) {
			return true;
		} else {
			//no duplicate found / the user is not yet registered in the db
			return false;
		}
	})
}

//User registration
/*
	Steps:
	1. create a new user object
	2. make sure that the password is encrypted
	3. save the new User to the database
*/

module.exports.registerUser = (req, res) => {
	let newUser = new User({
		firstName: req.body.firstName,
		lastName: req.body.lastName,
		email: req.body.email,
		mobileNo: req.body.mobileNo,
		password: bcrypt.hashSync(req.body.password, 10)
	})

	return newUser.save().then((user, error) => {
		if (error) {
			return res.send(false);
		} else {
			return res.send(true);
		}
	})

	.catch(err => res.send(err))
}

//User authentication
/*
	Steps:
	1. check if the db if user email exists
	2. compare the password from req.body and password stored in the db
	3. generate a JWT if the user logged in and return false if not
*/

module.exports.loginUser = (req, res) => {
	return User.findOne({email: req.body.email}).then(result => {
		if (result === null) {
			return false;
		} else {
			//compareSync method is used to compare a non-encrypted password and from the encrypted paswword from the db
			const isPasswordCorrect = bcrypt.compareSync(req.body.password, result.password)

			//if password match
			if (isPasswordCorrect) {
				return res.send({access: auth.createAccessToken(result)})
			} else {
				return res.send(false);
			}
		}
	})
	.catch(err => res.send(err))
}

//Get profile user
module.exports.getProfile = (req, res) => {
	return User.findById(req.user.id).then(result => {
		result.password = "";
		return res.send(result);
	})
	.catch(err => res.send(err))

};

// enroll user to a class
/*
	Steps:
	1. Find the document in the database using the user's ID
	2. Add the course ID to the user's enrollment array
	3. Update the document in the MongoDB Atlas database
*/
module.exports.enroll = async (req, res) => {

	// to check in the terminal the user id and the courseId
	console.log(req.user.id);
	console.log(req.body.courseId);

	// checks if the user is an admin and deny the enrollment
	if(req.user.isAdmin){
		return res.send("Action Forbidden");
	}

	// Creates an "isUserUpdated" variabe and returns true upon successful update otherwise returns error
	let isUserUpdated = await User.findById(req.user.id).then(user => {
		
		//Add the courseId in an object and push that object into the user's "enrollment" array
		let newEnrollment = {
			courseId: req.body.courseId
		}

		// Add the course in the "enrollments" array
		user.enrollments.push(newEnrollment);

		// Save the updated user and return true if successful or the error message if failed.
		return user.save().then(user => true).catch(err => err.message);


	});

	// Checks if there are error in updating the user
	if(isUserUpdated !== true){
		return res.send({ message: isUserUpdated});
	}


	let isCourseUpdated = await Course.findById(req.body.courseId).then(course => {

		let enrollee = {
			userId: req.user.id
		}

		course.enrollees.push(enrollee);

		return course.save().then(course => true).catch(err => err.message);
	})

	//checks if there are error in updating the course
	if(isCourseUpdated !== true){
		return res.send({ message: isCourseUpdated});
	}

	// Checkes if both user update and course update are successful.
	if(isUserUpdated && isCourseUpdated){
		return res.send({message: "Enrolled Successfully."})
	}
}

//getEnrollments of a user
module.exports.getEnrollments = async (req, res) => {
	return User.findById(req.user.id).then(result => {
		res.send(result.enrollments);
	})
}

// Function to reset the password
module.exports.resetPassword = async (req, res) => {
	try {

  	//used object destructuring
	const { newPassword } = req.body;
    const { id } = req.user; // Extracting user ID from the authorization header

    // Hashing the new password
    const hashedPassword = await bcrypt.hash(newPassword, 10);

    // Updating the user's password in the database
    await User.findByIdAndUpdate(id, { password: hashedPassword });

    // Sending a success response
    res.status(200).json({ message: 'Password reset successfully' });
  } catch (error) {
  	console.error(error);
  	res.status(500).json({ message: 'Internal server error' });
  }
};

// Controller function to update the user profile
module.exports.updateProfile = async (req, res) => {
	try {
    // Get the user ID from the authenticated token
		const id = req.user.id;

    // Retrieve the updated profile information from the request body
		const { firstName, lastName, mobileNo } = req.body;

    // Update the user's profile in the database
		const updatedUser = await User.findByIdAndUpdate(
			id,
			{ firstName, lastName, mobileNo },
			{ new: true }
			);

		res.json(updatedUser);
	} catch (error) {
		console.error(error);
		res.status(500).json({ message: 'Failed to update profile' });
	}
}


//updateUserAsAdmin
module.exports.updateUserAsAdmin = async (req, res) => {
	try {
		const { userId } = req.body;

		if (!userId) {
			return res.status(400).json({ message: 'User ID is required in the request body.' });
		}

	  const authenticatedUser = req.user; // Assuming authentication middleware is used

	  // Check if authenticated user is an admin
	  if (!authenticatedUser.isAdmin) {
	  	return res.status(403).json({ message: 'Access denied. Not an admin.' });
	  }

	  const userToUpdate = await User.findById(userId);

	  if (!userToUpdate) {
	  	return res.status(404).json({ message: 'User not found.' });
	  }

	  // Perform the admin update logic here, e.g. updating user's role to 'admin'
	  userToUpdate.isAdmin = true;
	  await userToUpdate.save();

	  return res.json({ message: 'User updated successfully.' });
	} catch (error) {
		console.error(error);
		return res.status(500).json({ message: 'An error occurred while updating the user.' });
	}
};

// Controller to update enrollment status of a user for a specific course
module.exports.updateEnrollmentStatus = async (req, res) => {
	try {
	  const { userId, courseId, status } = req.body;
  
	  if (!userId || !courseId || !status) {
		return res.status(400).json({ message: 'userId, courseId, and status are required in the request body.' });
	  }
  
	  /* const authenticatedUser = req.user; // Assuming authentication middleware is used
  
	  // Check if authenticated user is an admin
	  if (!authenticatedUser.isAdmin) {
		return res.status(403).json({ message: 'Access denied. Not an admin.' });
	  } */
	  
  
	  const user = await User.findById(userId);
  
	  if (!user) {
		return res.status(404).json({ message: 'User not found.' });
	  }
  
	  const enrollment = user.enrollments.find(enrollment => enrollment.courseId === courseId);
  
	  if (!enrollment) {
		return res.status(404).json({ message: 'Enrollment not found.' });
	  }
  
	  enrollment.status = status;
	  await user.save();
  
	  return res.json({ message: 'Enrollment status updated successfully.' });
	} catch (error) {
	  console.error(error);
	  return res.status(500).json({ message: 'An error occurred while updating enrollment status.' });
	}
  };
