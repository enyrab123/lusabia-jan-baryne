//Contain all the endpoints for our application

const express = require("express");

//Create a Router instance that functions as a middleware and routing system
const router = express.Router()

//use functions inside taskController.js
const taskController = require("../controllers/taskController")

//[Routes]
// Routes are responsible for defining the URI's that our client accesses and the corresponding controller functions that will be used when a route is accessed
//All the bussiness logic is done in the controller

router.get("/", (req, res) => {
	//user define getAllTasks()
	taskController.getAllTasks().then(resultFromController => res.send(resultFromController))
})

router.post("/", (req, res) => {
	//user define createTask()
	taskController.createTask(req.body).then(resultFromController => res.send(resultFromController))
})

//Route to delete a task
//This route expects to recieve a DELETE request at the "/task/:id"
//http://localhost:4000/tasks/:id
//task id is obtained from the URL is denoted by ":id" identifier in the route

router.delete("/:id", (req, res) => {
	taskController.deleteTask(req.params.id).then(resultFromController => res.send(resultFromController))
})

//Route to update a task
//This route expects to recieve a PUT request at the "/task/:id"
//http://localhost:4000/tasks/:id

router.put("/:id", (req, res) => {
	taskController.updateTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController))
})


//use module.exports to export the router object to use in index.js
module.exports = router