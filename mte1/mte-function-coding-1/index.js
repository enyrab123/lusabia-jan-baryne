function countLetter(letter, sentence) {
    // Check first whether the letter is a single character.
    // If letter is a single character, count how many times a letter has occurred in a given sentence then return count.
    // If letter is invalid, return undefined.

    let result = 0;

    if (letter.length === 1) {
        for (let x = 0; x < sentence.length; x++) {
            if (letter === sentence.charAt(x)) {
                result += 1;
            }
        }
    } else {
        result = undefined
    }

    return result;

} // end of countLetter


function isIsogram(text) {
    // An isogram is a word where there are no repeating letters.
    // The function should disregard text casing before doing anything else.
    // If the function finds a repeating letter, return false. Otherwise, return true.

    text = text.toLowerCase()
    
    for (let x = 0; x < text.length; x++) {
        for (let y = x + 1; y < text.length; y++) {
            if (text.charAt(x) === text.charAt(y)) {
                return false;
            }
        }
    }

    return true;

} // end of isIsogram

function purchase(age, price) {
    // Return undefined for people aged below 13.
    // Return the discounted price (rounded off) for students aged 13 to 21 and senior citizens. (20% discount)
    // Return the rounded off price for people aged 22 to 64.
    // The returned value should be a string.

    if (age < 13) {
        return undefined; 
    } else if (age >= 13 && age <= 21 || age >= 65) {
        const discountedPrice = (price * 0.8).toFixed(2); 

        return discountedPrice.toString(); 

    } else if (age >= 22 && age <= 64) {
        
        return price.toFixed(2).toString(); 
    }
    
}

function findHotCategories(items) {
    // Find categories that has no more stocks.
    // The hot categories must be unique; no repeating categories.

    // The passed items array from the test are the following:
    // { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' }
    // { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' }
    // { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' }
    // { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' }
    // { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }

    // The expected output after processing the items array is ['toiletries', 'gadgets'].
    // Only putting return ['toiletries', 'gadgets'] will not be counted as a passing test during manual checking of codes.

    const hotCategories = {};

    
    for (const item of items) {
        // Check if the item's stocks are zero
        if (item.stocks === 0) {
          hotCategories[item.category] = true;
        }
    }

    const hotCategoriesArray = Object.keys(hotCategories);

    return hotCategoriesArray;
}

function findFlyingVoters(candidateA, candidateB) {
    // Find voters who voted for both candidate A and candidate B.

    // The passed values from the test are the following:
    // candidateA: ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m']
    // candidateB: ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l']

    // The expected output after processing the candidates array is ['LIWf1l', 'V2hjZH'].
    // Only putting return ['LIWf1l', 'V2hjZH'] will not be counted as a passing test during manual checking of codes.


    const setA = new Set(candidateA);
    const setB = new Set(candidateB);

    const commonVoters = [...setA].filter(voter => setB.has(voter));

    return commonVoters;
}

module.exports = {
    countLetter,
    isIsogram,
    purchase,
    findHotCategories,
    findFlyingVoters
};