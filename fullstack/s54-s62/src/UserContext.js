import React from 'react';

// Creates a Context object

const UserContext = React.createContext();
// Allows us to share data between components (without using props)

// Provider component
export const UserProvider = UserContext.Provider;
// Provided data to components through the use of context

export default UserContext;