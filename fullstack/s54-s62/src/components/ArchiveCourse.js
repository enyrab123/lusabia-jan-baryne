import { useState } from 'react';
import { Button } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function ArchiveCourse({ course, fetchData, isActive }) {

	const archiveCourse = (e, courseId) => {

		e.preventDefault();

		fetch(`${ process.env.REACT_APP_API_URL}/courses/${courseId}/archive`, {

			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			}

		}) 
		.then(res => res.json())
		.then(data => {
			console.log(data);

			if (data === true) {
				Swal.fire({
					title: 'Success!',
					icon: 'success',
					text: 'Course Successfully Archived'
				})

				fetchData();

				
			} else {
				Swal.fire({
					title: 'Error!',
					icon: 'error',
					text: 'Please Try Again'
				})

				fetchData();

			}
		})
	} // end of archiveCourse

	const activateCourse = (e, courseId) => {
		e.preventDefault();

		fetch(`${ process.env.REACT_APP_API_URL}/courses/${courseId}/activate`, {

			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			}

		}) 
		.then(res => res.json())
		.then(data => {
			console.log(data);

			if (data === true) {
				Swal.fire({
					title: 'Success!',
					icon: 'success',
					text: 'Course Successfully Activated'
				})

				fetchData();

				
			} else {
				Swal.fire({
					title: 'Error!',
					icon: 'error',
					text: 'Please Try Again'
				})

				fetchData();

			}
		})
	} // end of activateCourse

	return (
		<>
			{isActive ?
				<Button variant="secondary" onClick={e => archiveCourse(e, course)}>Archive</Button>
				:
				<Button variant="success" onClick={e => activateCourse(e, course)}>Activate</Button>
			}
		</>

	) // end of return
} // end of function ArchiveCourse