import React from 'react';
import { Col, Card } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function Product(props) {

	// props is used here to get the data and breakPoint from the FeaturedCourses.js
	const { breakPoint, data } = props;

	const { _id, name, description, price } = data;

	return (
		<Col xs={12} md={breakPoint}>
			<Card className="cardHighlights mx-2">
				<Card.Body>
					<Card.Title className="text-center">
						<Link to="{`/courses/${_id}`">{name}</Link>
					</Card.Title>

					<Card.Text>{description}</Card.Text>
				</Card.Body>

				<Card.Footer>
					<h5 className="text-center">{price}</h5>
					<Link className="btn btn-orimary d-block" to="{`/courses/${_id}">Details</Link>
				</Card.Footer>
			</Card>                                                                                               
		</Col>
		


	) // end of return

} // end of function Product