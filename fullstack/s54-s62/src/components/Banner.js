import Button from 'react-bootstrap/Button';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { Link } from 'react-router-dom';

export default function Banner({ title, subtitle, btnText, linkTo }) {
	return (
		<Row>
			<Col>
				<h1>{title}</h1>
				<p>{subtitle}</p>
				<Link to={linkTo}>
				<Button variant="primary">{btnText}</Button>
				</Link>
			</Col>
		</Row>
	)
}