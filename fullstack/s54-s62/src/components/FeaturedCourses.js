import { useState, useEffect } from 'react';
import { CardGroup } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import PreviewCourses from './PreviewCourses'

export default function FeaturedCourses() {
	const [previews, setPreviews ] = useState([])

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/courses/`)
		.then(res => res.json())
		.then(data => {
			console.log(data)
			const numbers = []
			const featured = []

			const generateRandomNums = () => {
				let randomNum = Math.floor(Math.random() * data.length)

				if (numbers.indexOf(randomNum) === -1) {
					numbers.push(randomNum);
				} else {
					generateRandomNums()
				}
			} // end of generateRandomNums

			let courseToFeature = 0;

			if (data.length < 5) { 
				courseToFeature = data.length;   
			} else {
				courseToFeature = 5;
			}

			for (let i = 0; i < courseToFeature; i++) {
				generateRandomNums()

				featured.push(<PreviewCourses data={data[numbers[i]]} key={data[numbers[i]]._id} breakPoint={2} />)
			}


			setPreviews(featured)

		})
	}, []); // end of useEffect

	return (
		<>
			<h2 className="text-center">Feautured Courses</h2>
			<CardGroup className="justify-content-center">
				{previews}

			</CardGroup>
		</>
	) // end of return
} // end of function FeaturedCourses