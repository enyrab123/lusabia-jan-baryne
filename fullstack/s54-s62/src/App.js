
import { useState, useEffect } from 'react';
import Container from 'react-bootstrap/Container'
import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Routes } from 'react-router-dom';

import AppNavbar from './components/AppNavbar.js'
import AddCourse from './components/AddCourse'
import Home from './pages/Home'
import Profile from './pages/Profile'
import CourseView from './pages/CourseView'
import Error from './pages/Error'
import Courses from './pages/Courses'
import Register from './pages/Register'
import Login from './pages/Login'
import Logout from './pages/Logout'
import './App.css';
import { UserProvider } from './UserContext';

function App() {

  const [user, setUser] = useState({
    //token: localStorage.getItem('token')
    id: null,
    isAdmin: null
  })

  // Function for clearing localStorage on logout
  const unsetUser = () => {
    localStorage.clear();
  }

  // Used to check if the user information is properly stored upon login and the localStorage information is cleared upon logout
  useEffect(() => {
    //console.log(user);
    //console.log(localStorage);
    fetch(`${ process.env.REACT_APP_API_URL}/users/details`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(res => res.json())
    .then(data => {
      //console.log(data)

      // Set the user states values with the user details upon successful login.
      if (typeof data._id !== "undefined") {
        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        })
      } else {
        setUser({
          id: null,
          isAdmin: null
        })
      }

    })
  }, [user])

  return (
    // React Fragments <> </>
    <UserProvider value={{ user, setUser, unsetUser }}>
      <Router>

        <Container fluid>

          <AppNavbar />

          <Routes>

            <Route path="/" element={<Home/>} />
            <Route path="/profile" element={<Profile/>}/>
            <Route path="/courses" element={<Courses/>} />
            <Route exact path="/courses/:courseId" element={<CourseView/>} />
            <Route path="/register" element={<Register/>} />
            <Route path="/login" element={<Login/>} />
            <Route path="/logout" element={<Logout/>} />
            <Route path="/addCourse" element={<AddCourse/>} />
            <Route path="*" element={<Error/>} />

          </Routes>

        </Container>

      </Router>
    </UserProvider>
  );
}

export default App;
