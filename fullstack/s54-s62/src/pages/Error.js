import Banner from '../components/Banner.js'
import Highlights from '../components/Highlights.js'
import Button from 'react-bootstrap/Button';

export default function Error() {
	return (
		<div className='text-center mt-5'>
			<Banner
				title="404 - Not found"
				subtitle="The page you are looking for cannot be found"
				btnText="Back to home"
				linkTo="/"
			/>
		</div>
	)
}