import Banner from '../components/Banner.js'
import Highlights from '../components/Highlights.js'
import FeaturedCourses from '../components/FeaturedCourses.js'                                            

export default function Home() {                                                                     
	return (
		<div>
			<Banner
				title="Zuitt Coding Bootcamp"
				subtitle="Opportunities for everyone, everywhere."
				btnText="Enroll now!"
				linkTo="/"
			/>
			<FeaturedCourses />
			<Highlights />
		</div>
	) // end of return
} // end of function Home